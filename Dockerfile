# Ref1: http://tatamo.81.la/blog/2018/12/07/atcoder-cli-installation-guide/
# Ref2: http://tatamo.81.la/blog/2018/12/07/atcoder-cli-tutorial/

FROM ubuntu:18.04

# Install clang
# Ref: https://apt.llvm.org/
RUN apt update && apt install -y wget software-properties-common && \
    wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key| apt-key add - && \
    apt-add-repository "deb http://apt.llvm.org/bionic/ llvm-toolchain-bionic-10 main" && \
    apt install -y clang-10 lldb-10 lld-10 libc++-10-dev libc++abi-10-dev && \
    ln -s /usr/bin/clang++-10 /usr/bin/clang++ && \
    ln -s /usr/bin/clang-10 /usr/bin/clang

# Install Python and pip
RUN apt install -y python3.8 python3.8-distutils curl && \
    curl -o- https://bootstrap.pypa.io/get-pip.py | python3.8 && \
    pip3 install pip==20.3.1

# Add container user
# Ref1: https://qiita.com/0ashina0/items/1bd4b54f0e49a643e8a2
# Ref2: https://blog.nownabe.com/2018/06/05/1321.html/
ARG USERNAME=ubuntu
RUN apt install -y sudo &&  echo "${USERNAME}    ALL=(ALL)       NOPASSWD:ALL" >> /etc/sudoers && \
    groupadd -g 1000 ${USERNAME} && useradd -g 1000 -m -s /bin/bash -u 1000 ${USERNAME}

# Switch user
USER ${USERNAME}

# Install Node.js and npm
# Ref: https://stackoverflow.com/a/28390848
ENV NVM_DIR "/home/$USERNAME/.nvm"
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.37.2/install.sh | bash && \
    . $NVM_DIR/nvm.sh && nvm install v14.15.1 && npm install -g npm@6.14.9

# Install atcoder-cli and online-judge-tools
RUN . $NVM_DIR/nvm.sh && npm install -g atcoder-cli@2.1.0 && \
    pip3 install --user online-judge-tools==11.1.1 && \
    echo "export PATH=~/.local/bin:$PATH" >> ~/.bashrc

# Set up atcoder-cli
# Ref: https://blog.spiralray.net/cp/devenv-cpp
RUN . $NVM_DIR/nvm.sh && acc config default-task-choice all && \
    cd `acc config-dir` && mkdir cpp && \
    echo '{\n\t"task":{\n\t\t"program": ["main.cpp"],\n\t\t"submit": "main.cpp"\n\t}\n}' > cpp/template.json && \
    touch cpp/main.cpp && \
    acc config default-template cpp

CMD [ "sleep", "infinity" ]
