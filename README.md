# competitive-programming-container
Container environment for competitive programming, specially AtCoder.

## Dependency
* Docker (>=20.10.5)
* Docker Compose (>=1.26.0)
* Visual Studio Code (>=1.55.1)

## Setup
1. Execute `docker-compose up -d`
2. Access to container created at previous step from VS Code
3. Install recommended extensions
4. Login AtCoder with atcoder-cli
   * See [Usage page](https://github.com/Tatamo/atcoder-cli#usage)

## Usage
1. Download problems with `acc` command
2. Write code with VSCode
3. Compile and test code with `CheckTestCase` task
4. Submit code with `SubmitCode` task

## Author
abacus
